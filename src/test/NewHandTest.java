package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import poker.Card;
import poker.Handv2;

public class NewHandTest {
// We have two constructors out of list and string. If one does the same thing
//	as another, there is no problem. Equals: same cards AND value!
	@Test
	public void shouldGiveEqualHandsForTwoConstructorMethods() {
		//given
		Handv2 Handv21 = new Handv2("AC 2H 7S KD 3C");
		ArrayList<Card> list= new ArrayList<Card>();
		list.add(new Card("AC"));
		list.add(new Card("2H"));
		list.add(new Card("7S"));
		list.add(new Card("KD"));
		list.add(new Card("3C"));
		Handv2 Handv22 = new Handv2(list);
		//when
		boolean equal=Handv21.equals(Handv22);
		//then
		assertEquals(true, equal);
	}
	@Test
	public void shouldThrowExceptionHandTooLong() {
		//given
		boolean exceptionCaught=false;
		try {
			Handv2 tooLong = new Handv2("AC QD 2H 7S KD 3C");
		}//when
		catch (IllegalArgumentException e) {
			exceptionCaught = true;
			System.out.println(e.toString());
		}
		//then
		assertEquals(true, exceptionCaught);
	}
	
	@Test
	public void shouldConstructIdenticalHandsWithTwoMethods() {
		//given
		boolean exceptionCaught=false;
		try {
			Handv2 tooLong = new Handv2("AC QD 2H 7S KD 3C");
		}//when
		catch (IllegalArgumentException e) {
			exceptionCaught = true;
			System.out.println(e.toString());
		}
		//then
		assertEquals(true, exceptionCaught);
	}
	
	@Test
	public void shouldThrowExceptionHandTooShort() {
		//given
		boolean exceptionCaught=false;
		try {
			Handv2 tooShort = new Handv2("AC QD 2H 7S");
		}//when
		catch (IllegalArgumentException e) {
			exceptionCaught = true;
			System.out.println(e.toString());
		}
		//then
		assertEquals(true, exceptionCaught);
	}
	
	@Test
	public void shouldThrowExceptionBadCard() {
		//given
		boolean exceptionCaught=false;
		try {
			Handv2 badCard = new Handv2("AC QD 2H 7S XX");
		}//when
		catch (IllegalArgumentException e) {
			exceptionCaught = true;
			System.out.println(e.toString());
		}
		//then
		assertEquals(true, exceptionCaught);
	}
	
	@Test
	public void shouldThrowExceptionSameCards() {
		//given
		boolean exceptionCaught=false;
		try {
			Handv2 cheater = new Handv2("AC AC AH AS AD");
		}//when
		catch (IllegalArgumentException e) {
			exceptionCaught = true;
			System.out.println(e.toString());
		}
		//then
		assertEquals(true, exceptionCaught);
	}
	
	@Test
	public void shouldGiveWinnerHighCardVsHighCardFirstCardDecides() {
		//given
		Handv2 higher= new Handv2("AC QH 8D JH KS");
		Handv2 lower= new Handv2("TC QH 8D JH KS");
		//when
		boolean gameResult = higher.compareTo(lower)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerHighCardVsHighCardSecondCardDecides() {
		//given
		Handv2 higher= new Handv2("AC QH 8D JH KS");
		Handv2 lower= new Handv2("AC QH 8D JH TS");
		//when
		boolean gameResult = higher.compareTo(lower)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerHighCardVsHighCardThirdCardDecides() {
		//given
		Handv2 higher= new Handv2("AC QH 8D JH KS");
		Handv2 lower= new Handv2("AC TH 8D JH KS");
		//when
		boolean gameResult = higher.compareTo(lower)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerHighCardVsHighCardFourthCardDecides() {
		//given
		Handv2 higher= new Handv2("AC QH 8D JH KS");
		Handv2 lower= new Handv2("AC QH 8D TH KS");
		//when
		boolean gameResult = higher.compareTo(lower)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerHighCardVsHighCardLastCardDecides() {
		//given
		Handv2 higher= new Handv2("AC QH 9D JH KS");
		Handv2 lower= new Handv2("AC QH 8D JH KS");
		//when
		boolean gameResult = higher.compareTo(lower)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerPairVsHighCard() {
		//given
		Handv2 pair= new Handv2("2C 2H 9D JH KS");
		Handv2 highCard= new Handv2("AC QH 9D JH KS");
		//when
		boolean gameResult = pair.compareTo(highCard)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerPairVsPairPairDecides() {
		//given
		Handv2 higherPair= new Handv2("AC AH 9D JH KS");
		Handv2 lowerPair= new Handv2("KD KS 8D JH 3S");
		//when
		boolean gameResult = higherPair.compareTo(lowerPair)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerPairVsPairFirstCardDecides() {
		//given
		Handv2 higherPair= new Handv2("AC AH 9D JH KS");
		Handv2 lowerPair= new Handv2("AD AS 9D JH QS");
		//when
		boolean gameResult = higherPair.compareTo(lowerPair)>0;
		//then
		assertEquals(true, gameResult );
	}	
	
	@Test
	public void shouldGiveWinnerPairVsPairSecondCardDecides() {
		//given
		Handv2 higherPair= new Handv2("AC AH 9D JH KS");
		Handv2 lowerPair= new Handv2("AD AS 9D TH KS");
		//when
		boolean gameResult = higherPair.compareTo(lowerPair)>0;
		//then
		assertEquals(true, gameResult );
	}	
	
	@Test
	public void shouldGiveWinnerPairVsPairLastCardDecides() {
		//given
		Handv2 higherPair= new Handv2("AC AH 9D JH KS");
		Handv2 lowerPair= new Handv2("AD AS 8D JH KS");
		//when
		boolean gameResult = higherPair.compareTo(lowerPair)>0;
		//then
		assertEquals(true, gameResult );
	}	
	
	@Test
	public void shouldGiveWinnerTwoPairsVsPair() {
		//given
		Handv2 twoPairs= new Handv2("AC AH 3D 2H 2S");
		Handv2 pair= new Handv2("AD AS 8D JH KS");
		//when
		boolean gameResult = twoPairs.compareTo(pair)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerTwoPairsVsTwoPairsFirstPairDecides() {
		//given
		Handv2 higherPairs= new Handv2("AC AH 3D 2H 2S");
		Handv2 lowerPairs= new Handv2("KD KS 8D QH QS");
		//when
		boolean gameResult = higherPairs.compareTo(lowerPairs)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerTwoPairsVsTwoPairsSecondPairDecides() {
		//given
		Handv2 higherPairs= new Handv2("AC AH 3D 4H 4S");
		Handv2 lowerPairs= new Handv2("AD AS 8D 3C 3D");
		//when
		boolean gameResult = higherPairs.compareTo(lowerPairs)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerTwoPairsVsTwoPairsLastCardDecides() {
		//given
		Handv2 higherPairs= new Handv2("AC AH 3D 4H 4S");
		Handv2 lowerPairs= new Handv2("AD AS 2D 4C 4D");
		//when
		boolean gameResult = higherPairs.compareTo(lowerPairs)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerThreeVsTwoPairs() {
		//given
		Handv2 three= new Handv2("4C 5H 3D 4H 4S");
		Handv2 twoPairs= new Handv2("AD AS QD KC KD");
		//when
		boolean gameResult = three.compareTo(twoPairs)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerThreeVsThree() {
		//given
		Handv2 higherThree= new Handv2("4C 5H 3D 4H 4S");
		Handv2 lowerThree= new Handv2("3H 3S QD 3C KD");
		//when
		boolean gameResult = higherThree.compareTo(lowerThree)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerLowestStraitVsThree() {
		//given
		Handv2 lowestStrait= new Handv2("AC 5H 3D 2H 4S");
		Handv2 three= new Handv2("AH AS QD 3C AD");
		//when
		boolean gameResult = lowestStrait.compareTo(three)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerStraitVsLowestStrait() {
		//given
		Handv2 strait= new Handv2("2C 3H 4D 6H 5S");
		Handv2 lowestStrait= new Handv2("AC 5H 3D 2H 4S");
		//when
		boolean gameResult = strait.compareTo(lowestStrait)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerFlushVsStrait() {
		//given
		Handv2 flush= new Handv2("2S 3S 4S 6S 7S");
		Handv2 strait= new Handv2("AC QH KD JH TS");
		//when
		boolean gameResult = flush.compareTo(strait)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerFlushVsFlush() {
		//given
		Handv2 higherFlush= new Handv2("2S 3S 4S 6S 8S");
		Handv2 lowerFlush= new Handv2("2C 3C 4C 6C 7C");
		//when
		boolean gameResult = higherFlush.compareTo(lowerFlush)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerFullVsFlush() {
		//given
		Handv2 full= new Handv2("2S 2D 2H 3D 3S");
		Handv2 flush= new Handv2("2C JC KC QC AC");
		//when
		boolean gameResult = full.compareTo(flush)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerFullVsFull() {
		//given
		Handv2 higherFull= new Handv2("2S 2D 4H 4D 4S");
		Handv2 lowerFull= new Handv2("2C 2H 3C 3S 3D");
		//when
		boolean gameResult = higherFull.compareTo(lowerFull)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerQuadVsFull() {
		//given
		Handv2 quad= new Handv2("2C 2D 2H 4D 2S");
		Handv2 full= new Handv2("KC KH AC AS AD");
		//when
		boolean gameResult = quad.compareTo(full)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerQuadVsQuad() {
		//given
		Handv2 higherQuad= new Handv2("QC QD QH 4D QS");
		Handv2 lowerQuad= new Handv2("KC JH JC JS JD");
		//when
		boolean gameResult = higherQuad.compareTo(lowerQuad)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerLowestStraitFlushVsQuad() {
		//given
		Handv2 lowestStraitFlush= new Handv2("AC 2C 3C 4C 5C");
		Handv2 quad= new Handv2("KC KH 7C KS KD");
		//when
		boolean gameResult = lowestStraitFlush.compareTo(quad)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerStraitFlushVsLowestStraitFlush() {
		//given
		Handv2 straitFlush= new Handv2("2H 3H 4H 5H 6H");
		Handv2 lowestStraitFlush= new Handv2("AC 2C 3C 4C 5C");
		//when
		boolean gameResult = straitFlush.compareTo(lowestStraitFlush)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerStraitFlushVsStraitFlush() {
		//given
		Handv2 higherStraitFlush= new Handv2("3C 4C 5C 6C 7C");
		Handv2 lowerStraitFlush= new Handv2("2H 3H 4H 5H 6H");
		//when
		boolean gameResult = higherStraitFlush.compareTo(lowerStraitFlush)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveWinnerRoyalStraitFlushVsStraitFlush() {
		//given
		Handv2 royalStraitFlush= new Handv2("TC JC KC QC AC");
		Handv2 straitFlush= new Handv2("9H TH KH QH JH");
		//when
		boolean gameResult = royalStraitFlush.compareTo(straitFlush)>0;
		//then
		assertEquals(true, gameResult );
	}
	
	@Test
	public void shouldGiveAtie() {
		//given
		Handv2 royalStraitFlush1= new Handv2("TC JC KC QC AC");
		Handv2 royalStraitFlush2= new Handv2("TS JS KS QS AS");
		
		//when
		boolean gameResult = royalStraitFlush1.compareTo(royalStraitFlush2)==0;
		//then
		assertEquals(true, gameResult );
	}
	
	
}
