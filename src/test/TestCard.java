package test;

import static org.junit.Assert.*;

import org.junit.Test;

import poker.Card;

public class TestCard {

	@Test
	public void shouldConstructAceofSpadesofString() {
		//given
		Card card = new Card("AS");
		//when
		String result=card.toString();
		//then
		assertEquals("AS",result);
	}
	
	@Test public void shouldCompareRanks() {
		//given
		Card card1 = new Card("TH");
		Card card2 = new Card("JC");
		//when
		boolean result = card1.compareTo(card2)<0;
		//then
		assertEquals(true,result);
	}
	
	@Test public void shouldCompareEquals() {
		//given
		Card card1 = new Card("2D");
		Card card2 = new Card("2D");
		//then
		assertEquals(true,card1.equals(card2));
	}
	
	@Test public void shouldThrowExceptionOnWrongcard() {
		//given
		boolean exceptionCaught=false;
		try{
			Card card1 = new Card("1F");
		}
		catch( Exception e)
		{
			String result = e.toString();
			System.out.println(result);
			exceptionCaught=true;
		}
		assertEquals(true,exceptionCaught);
	}

}
