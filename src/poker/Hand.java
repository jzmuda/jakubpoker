package poker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * First principle: one can define the value (score) of a hand of five cards with one unique number.
 * Comparator of two hands = comparator of two numbers.
 * approximately ("highest card" is the sequence highest card in sequences):
 * number = 13**0 * lowest card +...+13**4 * highest card + 13**5 * bonus sequence (flush, full etc.)
 * 
 * Method with hand cards sorting. Quicker hand evaluation from first and subsequent principles.
 * In sequence highest rank goes to the right.
 * 
 * @author JZMUDA
 *
 * Sorting gives some distinguishable card sequences :
 * 
 * 1 1 1 1 1 - length 5 in highest rank, strait, flush, strait flush and royal flush. First most common
 *				
 * 2 1 1 1
 * 1 2 1 1 - length 4, four possibilities, one pair case exquisite.
 * 1 1 2 1
 * 1 1 1 2
 * 
 * 2 2 1
 * 2 1 2
 * 1 2 1
 * or	 - length 3, two pairs or three. Distinguishable by numbers (for three leading card in the middle)
 * 3 1 1
 * 1 3 1
 * 1 1 3
 * 
 * 2 3
 * 3 2
 * or   - full house or quad. Both cases leading card landed in the middle of the hand after sorting.
 * 4 1
 * 1 4
 * 
 * One can split one ugly function with hundred loops and if cases to few simple subroutines according
 * to the length and content of the found structure. 
 **/

public class Hand implements Comparable<Hand>{
	private List<Card> hand;
	private List<Integer> sequence;
	private int value = 0;
	private int longestSequence = 0;
	private boolean isFlush = true;
	private boolean isStrait = true;
	//Multipliers for card sequence (pair, three etc.) evaluation
	final static int subsubleadingMultiplier=13*13*13; //13**3
	final static int subleadingMultiplier=subsubleadingMultiplier*13; //13**4
	final static int leadingMultiplier=subleadingMultiplier*13;//13**5
	
	public int getValue() {
		return value;
	}
	
	public Hand(List<Card> cards) {
		if(cards.size()==5) {
			isFlush = true;
			isStrait = true;
			value = 0;
			hand = new ArrayList<Card>();
			for(Card card: cards)
				this.hand.add(card);
			Collections.sort(this.hand);
			findSequence();
			setSequencedValue();
		}
		else throw new IllegalArgumentException("Hand should consist of five cards");
	}
	
	public Hand(String cards) {
		longestSequence = 0;
		isFlush = true;
		isStrait = true;
		String[] splited = cards.split(" ");
		if (splited.length!=5)
			throw new IllegalArgumentException("Hand should consist of five cards");
		hand = new ArrayList<Card>();
		for(int i = 0; i < 5 ; i++)
			hand.add(new Card(splited[i]));
		Collections.sort(this.hand);
		findSequence();
		setSequencedValue();
	}
	
	public String toString() {
		String result = "";
		for(Card card : hand)
			result = result + card.toString() + " ";
		return result;
	}
	//One can do a single linear search to count most popular highest card value,
	//search for flush and strait, longest sequence and construct the sequence vector.
	//This one function does evaluate ~52% of possible poker hands, aggressive optimization.
	private void findSequence()
	{
		int multiplier=13;
		sequence = new ArrayList<Integer>();
		sequence.add(1);
		int previousRank=hand.get(0).getRank();
		int previousSuit=hand.get(0).getSuit();
		value=previousRank;
		int j=0;
		for(int i=1; i<hand.size(); i++) {
			int currentRank=hand.get(i).getRank();
			int currentSuit=hand.get(i).getSuit();
			if(currentRank==previousRank) {
				updateSequenceLength(j);
				if (currentSuit==previousSuit) throw new IllegalArgumentException("Cheater!");
			}
			else {
				sequence.add(1);
				j++;
				//An Ace can count as the lowest card in strait family...
				if( isLowestStrait(i,currentRank,previousRank)){
					previousRank=currentRank-1;
					value-=currentRank*multiplier;
				}
			}
			checkStrait(currentRank, previousRank);
			checkFlush(currentSuit, previousSuit);
			value+=currentRank*multiplier;//card sequence value as a number. Highest card most important etc.
			multiplier*=13;
			previousRank=currentRank;
			previousSuit=currentSuit;
		}
	}
	
	private boolean isLowestStrait(int i, int currentRank, int previousRank) {
		return (isStrait==true)&&(i==4)&&(currentRank==12)&&(previousRank==3);
	}
	
	private void updateSequenceLength(int j){
		sequence.set(j, sequence.get(j)+1);
		if(sequence.get(j)>longestSequence) longestSequence=sequence.get(j);
	}
	
	private void checkStrait(int currentRank, int previousRank){
		if(currentRank!=previousRank+1)
			isStrait=false;
	}
	private void checkFlush(int currentSuit, int previousSuit){
		if(currentSuit!=previousSuit)
			isFlush=false;
	}
	
	//corrects the value if a card sequence (strait, flush, pair, two , three, full or quad) present
	private void setSequencedValue(){
		switch(sequence.size()) {
		case 5: addForFlushAndStrait();
		break;
		case 4: doOnePair();
		break;
		case 3: doTwoPairsOrThree();
		break;
		case 2: doFullOrQuad();
		break;
		default:
			throw new IllegalStateException("Algorithm Failure");
		}
	}
	
	private void addForFlushAndStrait() {
		if (isStrait)
		{
			value+=4*leadingMultiplier;
			if (isFlush)
				value+=4*leadingMultiplier;
		}
		if(isFlush) value+=5*leadingMultiplier;
	}
	
	//~42% of cases. Pair value most important, then other card values
	private void doOnePair() {
		value=leadingMultiplier;//bonus for a pair
		int multiplier=13;
		int j=0;
		for(int i=0; i<sequence.size(); i++) {
			int rank=hand.get(j).getRank();
			if(sequence.get(i)==2) {
				value+=rank*subleadingMultiplier;//pair value second most important information
				j+=2;
			}
			else {
				value+=rank*multiplier;//other cards as they appear (no pair card double-counting)
				multiplier*=13;
				j++;
			}
		}
	}
	//if its three we need only the value of one of the cards from three. Two pairs more complicated
	
	private void doTwoPairsOrThree() {
		if (longestSequence==2)
			doTwoPairs();
		else doThree();
	}
	
	private void doTwoPairs() {
		int j=0;
		value=0;
		int pairmul=subsubleadingMultiplier;//first pair of two
		for(int i=0; i<sequence.size(); i++) {
			int length=sequence.get(i);
			int rank=hand.get(j).getRank();
			if(length==2) {
				value+=(leadingMultiplier + pairmul * rank);//half of the two pair bonus plus pair rank
				pairmul*=13;//second pair in a sorted hand will have higher rank
				j+=2;
			}
			else{
				value+=rank*13*13; //the unpaired card in case of two pairs
				j++;
			}
		}
	}
	//After sorting one can read the rank of three, full or quad from the middle card
	private void doThree() {
		value=(3*leadingMultiplier+hand.get(2).getRank()*subleadingMultiplier);
	}
	
	private void doFullOrQuad() {
		if(longestSequence==4)
			doQuad();
		else
			doFull();
	}
	
	private void doQuad() {
		value=7*leadingMultiplier + hand.get(2).getRank()*subleadingMultiplier;
	}
	
	private void doFull() {
		value=6*leadingMultiplier + hand.get(2).getRank()*subleadingMultiplier;
	}
	
	public int compareTo(Hand other) {
		int result= this.getValue()-other.getValue();
		return result;
	}
	
	public List<Card> getCards() {
		return hand;
	}
	
	public boolean equals(Hand other) {
		if( this==other ) return true;
		
		if(other==null) return false;
		
		if (getClass() != other.getClass())
			return false;
		//same value and cards
		List<Card> otherCards=other.getCards();
		boolean result=true;
		for(int i=0; i< 5; i++) {
			if (!hand.get(i).equals(otherCards.get(i)))
				result=false;
		}
		return (result && value==other.getValue());
	}
}
