package poker;

import java.util.HashMap;

public class Card implements Comparable<Card> {
	private static HashMap<String,Integer> suitMap = new HashMap<String, Integer>();
	static {
		suitMap.put("C",0);
		suitMap.put("S",1);
		suitMap.put("D",2);
		suitMap.put("H",3);
	}
	
	private static HashMap<String, Integer> rankMap= new HashMap<String, Integer>();
    static {
    	rankMap.put("2",0);
    	rankMap.put("3",1);
    	rankMap.put("4",2);
    	rankMap.put("5",3);
    	rankMap.put("6",4);
    	rankMap.put("7",5);
    	rankMap.put("8",6);
    	rankMap.put("9",7);
    	rankMap.put("T",8);
    	rankMap.put("J",9);
    	rankMap.put("Q",10);
    	rankMap.put("K",11);
    	rankMap.put("A",12);
    	
    }
	private int rank;
	private int suit;
	
	private static String[] suits = {
			"C", "S","D","H"
	};
	
	private static String[] ranks = {
			"2","3","4","5","6","7","8","9","T","J","Q","K","A"
	};
	
	public Card(int i, int j){
		this.rank= i%13;
		this.suit= j%4;
	}
	
	public Card(String carta){
		try {
			this.rank=rankMap.get(carta.charAt(0)+"");
			this.suit=suitMap.get(carta.charAt(1)+"");
		}
		catch(Exception e) {
			throw new IllegalArgumentException("No such card "+carta);
		}
	}
	
	public int getSuit(){
		return this.suit;
	}
	
	public int getRank(){
		return this.rank;
	}
	
	public String toString() {
		return ranks[rank]+suits[suit];
	}
	
	public int compareTo(Card other){
		return (10*this.getRank() + this.getSuit() - 10*other.getRank()-other.getSuit()); 
	}
	
	public boolean equals(Card other){
		if( this==other )
			return true;

		if(other==null)
			return false;

		if (getClass() != other.getClass())
			return false;
		
		return (this.rank==other.getRank() && this.suit == other.getSuit());
	}
}
