package poker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * 
 * 
 * 
 */

public class Handv2 implements Comparable<Handv2>{
	private List<Card> hand;
	private List<Integer> sequence;
	private int longestSequence = 0;
	private boolean isFlush = true;
	private boolean isStrait = true;
	public static enum sequences {HIGH_CARD,PAIR,TWO_PAIRS, THREE, STRAIT, FLUSH, FULL, FOUR, STRAIT_FLUSH};
	private sequences seq;
	
	public Handv2(List<Card> cards) {
		if(cards.size()==5) {
			isFlush = true;
			isStrait = true;
			hand = new ArrayList<Card>();
			for(Card card: cards)
				this.hand.add(card);
			Collections.sort(this.hand);
			findSequence();
			setSequencedValue();
		}
		else throw new IllegalArgumentException("Hand should consist of five cards");
	}
	
	public Handv2(String cards) {
		longestSequence = 0;
		isFlush = true;
		isStrait = true;
		String[] splited = cards.split(" ");
		if (splited.length!=5)
			throw new IllegalArgumentException("Hand should consist of five cards");
		hand = new ArrayList<Card>();
		for(int i = 0; i < 5 ; i++)
			hand.add(new Card(splited[i]));
		Collections.sort(this.hand);
		findSequence();
		setSequencedValue();
	}
	
	public String toString() {
		String result = "";
		for(Card card : hand)
			result = result + card.toString() + " ";
		return result;
	}
	//One can do a single linear search to count most popular highest card value,
	//search for flush and strait, longest sequence and construct the sequence vector.
	//This one function does evaluate ~52% of possible poker hands, aggressive optimization.
	private void findSequence()
	{
		sequence = new ArrayList<Integer>();
		sequence.add(1);
		int previousRank=hand.get(0).getRank();
		int previousSuit=hand.get(0).getSuit();
		int j=0;
		for(int i=1; i<hand.size(); i++) {
			int currentRank=hand.get(i).getRank();
			int currentSuit=hand.get(i).getSuit();
			if(currentRank==previousRank) {
				updateSequenceLength(j);
				if (currentSuit==previousSuit) throw new IllegalArgumentException("Cheater!");
			}
			else {
				sequence.add(1);
				j++;
				//An Ace can count as the lowest card in strait family...
				if( isLowestStrait(i,currentRank,previousRank)){
					previousRank=currentRank-1;
				}
			}
			checkStrait(currentRank, previousRank);
			checkFlush(currentSuit, previousSuit);
			previousRank=currentRank;
			previousSuit=currentSuit;
		}
	}
	
	private boolean isLowestStrait(int i, int currentRank, int previousRank) {
		return (isStrait==true)&&(i==4)&&(currentRank==12)&&(previousRank==3);
	}
	
	private void updateSequenceLength(int j){
		sequence.set(j, sequence.get(j)+1);
		if(sequence.get(j)>longestSequence) longestSequence=sequence.get(j);
	}
	
	private void checkStrait(int currentRank, int previousRank){
		if(currentRank!=previousRank+1)
			isStrait=false;
	}
	private void checkFlush(int currentSuit, int previousSuit){
		if(currentSuit!=previousSuit)
			isFlush=false;
	}
	
	//corrects the value if a card sequence (strait, flush, pair, two , three, full or quad) present
	private void setSequencedValue(){
		switch(sequence.size()) {
		case 5: setFlushAndStrait();
		break;
		case 4: this.seq = sequences.PAIR;
		break;
		case 3: setTwoPairsOrThree();
		break;
		case 2: setFullOrFour();
		break;
		default:
			throw new IllegalStateException("Algorithm Failure");
		}
	}
	
	private void setFlushAndStrait() {
		if (isStrait)
		{
			this.seq = sequences.STRAIT;
			if (isFlush)
				this.seq = sequences.STRAIT_FLUSH;
		}
		else if(isFlush)
			this.seq = sequences.FLUSH;
		else this.seq = sequences.HIGH_CARD;
	}
	
	//~42% of cases. Pair value most important, then other card values
		private void setOnePair() {
			
		}
		
	private void setTwoPairsOrThree() {
		if (longestSequence==2)
			this.seq = sequences.TWO_PAIRS;
		else this.seq = sequences.THREE;
	}
	
	private void setFullOrFour() {
		if(longestSequence==4)
			this.seq = sequences.FOUR;
		else
			this.seq = sequences.FULL;
	}
	//if its three we need only the value of one of the cards from three. Two pairs more complicated
	
	
	private void doTwoPairs() {
		int j=0;
		for(int i=0; i<sequence.size(); i++) {
			int length=sequence.get(i);
			int rank=hand.get(j).getRank();
			if(length==2) {
				j+=2;
			}
			else{
				j++;
			}
		}
	}
	
	public int compareTo(Handv2 other) {
		//does the sequence decide?
		int result=seq.compareTo(other.getSequence());
		if (result==0) {
			switch (seq) {
			case HIGH_CARD:
			case FLUSH:			
				result = evaluateHighCard(other.getCards());
				break;
			case PAIR:
				result = evaluateOnePair(other.getCards());
				break;
			case TWO_PAIRS:
				result = evaluateTwoPairs(other.getCards());
				break;
			case THREE:
			case FOUR:
			case FULL:
				result = evaluateThreeOrFour(other.getCards());
				break;
			case STRAIT:
			case STRAIT_FLUSH:
				result = evaluateStraits(other.getCards());
				break;
			}
		}
		return result;
	}

	public sequences getSequence() {
		return seq;
	}
	
	public List<Card> getCards() {
		return hand;
	}
	
	
	private int evaluateTwoPairs(List<Card> other) {
		int thisRank=0;
		int otherRank=0;
		int result=0;
		//search highest pair
		for(int i=4; i>0; i--) {
			if(thisRank==0 && hand.get(i).getRank()==hand.get(i-1).getRank())
				thisRank=hand.get(i).getRank();
			if(otherRank==0 && other.get(i).getRank()==other.get(i-1).getRank())
				otherRank=other.get(i).getRank();
		}
		result=thisRank-otherRank;
		if (result==0)//if not first pair then the second pair (searched from below)
			result=evaluateOnePair(other);
		return result;
	}
	
	//~42% of cases. Pair value most important, then other card values
	private int evaluateOnePair(List<Card> other) {
		int thisRank=0;
		int otherRank=0;
		int result=0;
		for(int i=0; i<4; i++) { //extra check if we already have a pair. In two pair case this returns the lower one.
			if(thisRank==0 && hand.get(i).getRank()==hand.get(i+1).getRank())
				thisRank=hand.get(i).getRank();
			if(otherRank==0 && other.get(i).getRank()==other.get(i+1).getRank())
				otherRank=other.get(i).getRank();
		}
		result=thisRank-otherRank;
		if (result==0)//if pairs equal highest card decides
			result=evaluateHighCard(other);
		return result;
	}
	
	private int evaluateHighCard(List<Card> other) {
		int result =0;
		for (int i=4; i >=0; i--) {
			if(hand.get(i).getRank()>other.get(i).getRank()) {
				result=1;
				i=-1;
			}
			else if(hand.get(i).getRank()<other.get(i).getRank()) {
				result=-1;
				i=-1;
			}
		}
		return result;
	}
	
	private int evaluateThreeOrFour(List<Card> other) {
		// TODO Auto-generated method stub
		return hand.get(2).getRank()-other.get(2).getRank();
	}
	
	private int evaluateStraits(List<Card> other) {
		//sorted flushes - e .g. middle card can decide
		int result = hand.get(2).getRank()-other.get(2).getRank();
		//but sometimes it goes up to ace and it counts as one
		if (result==0)
			result=other.get(4).getRank()-hand.get(4).getRank();
		return result;
	}
	
	
	
	public boolean equals(Handv2 other) {
		if( this==other ) return true;
		
		if(other==null) return false;
		
		if (getClass() != other.getClass())
			return false;
		//same value and cards
		List<Card> otherCards=other.getCards();
		boolean result=true;
		for(int i=0; i< 5; i++) {
			if (!hand.get(i).equals(otherCards.get(i)))
				result=false;
		}
		return (result && this.compareTo(other)==0);
		//return (result && value==other.getValue());
	}
}
