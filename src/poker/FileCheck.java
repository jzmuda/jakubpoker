package poker;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class FileCheck {
	private int playerOneScore=0;
	private int playerTwoScore=0;
	private int ties=0;
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FileCheck file=new FileCheck();
		file.ReadFile();
		file.printScore();
	}

	void ReadFile() {
		try {


			Path currentRelativePath = Paths.get("");
			String fileName = currentRelativePath.toAbsolutePath().toString();
			fileName+="\\src\\poker\\poker.txt";
			File file = new File(fileName);
			Scanner input = new Scanner(file);


			while (input.hasNextLine()) {
				String line = input.nextLine();
				String[] splited = line.split(" ");
				ArrayList<Card> playerOneHand = new ArrayList<Card>();
				for(int i = 0; i < 5 ; i++)
					playerOneHand.add(new Card(splited[i]));
				ArrayList<Card> playerTwoHand = new ArrayList<Card>();
				for(int i = 5; i < 10 ; i++)
					playerTwoHand.add(new Card(splited[i]));
				//System.out.println(line);
				Handv2 playerOne = new Handv2(playerOneHand);
				Handv2 playerTwo = new Handv2(playerTwoHand);
				if (playerOne.compareTo(playerTwo) >0) playerOneScore++;
				else if (playerOne.compareTo(playerTwo) <0) playerTwoScore++;
				else ties++;
			}
			input.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void printScore() {
		System.out.println("Player One Score: " + playerOneScore + " Player Two Score: " + playerTwoScore +" Ties: "+ties);
	}
	

}
